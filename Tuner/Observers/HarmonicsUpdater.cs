﻿using System;
using System.Collections.Generic;

namespace Tuner.Observers
{
    public class HarmonicsUpdater : IHarmonicsObserver
    {
        public event EventHandler<List<Harmonic>> Updated;
        private List<Harmonic> harmonics;

        public HarmonicsUpdater()
        {
            harmonics = new List<Harmonic>();
        }

        public void ModifyObservedItem(List<Harmonic> harmonics)
        {
            this.harmonics = harmonics;
            Updated?.Invoke(this, harmonics);
        }
    }

    public interface IHarmonicsObserver
    {
        event EventHandler<List<Harmonic>> Updated;
    }

}
