﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace Tuner
{
    [Register ("FirstViewController")]
    partial class FirstViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton recordButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel tuneFrequencyLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel tuneNameLabel { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (recordButton != null) {
                recordButton.Dispose ();
                recordButton = null;
            }

            if (tuneFrequencyLabel != null) {
                tuneFrequencyLabel.Dispose ();
                tuneFrequencyLabel = null;
            }

            if (tuneNameLabel != null) {
                tuneNameLabel.Dispose ();
                tuneNameLabel = null;
            }
        }
    }
}