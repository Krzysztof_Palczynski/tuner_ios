﻿using System;
using System.Collections.Generic;
using Tuner.Services.TextProcessing;

namespace Tuner.Services.SoundProcessing
{
    public static class AcousticSounds
    {
        public static Dictionary<Sounds, double> Sounds_432Hz = new Dictionary<Sounds, double>()
        {
            {   Sounds. D2, 72.08   },
            {   Sounds.         DSharp2_Eb2,    76.37   },
            {   Sounds.         E2, 80.91   },
            {   Sounds.         F2, 85.72   },
            {   Sounds.         FSharp2_Gb2,    90.82   },
            {   Sounds.         G2, 96.22   },
            {   Sounds.         GSharp2_Ab2,    101.94  },
            {   Sounds.         A2, 108.00  },
            {   Sounds.         ASharp2_Bb2,    114.42  },
            {   Sounds.         B2, 121.23  },
            {   Sounds.         C3, 128.43  },
            {   Sounds.         CSharp3_Db3,    136.07  },
            {   Sounds.         D3, 144.16  },
            {   Sounds.         DSharp3_Eb3,    152.74  },
            {   Sounds.         E3, 161.82  },
            {   Sounds.         F3, 171.44  },
            {   Sounds.         FSharp3_Gb3,    181.63  },
            {   Sounds.         G3, 192.43  },
            {   Sounds.         GSharp3_Ab3,    203.88  },
            {   Sounds.         A3, 216.00  },
            {   Sounds.         ASharp3_Bb3,    228.84  },
            {   Sounds.         B3, 242.45  },
            {   Sounds.         C4, 256.87  },
            {   Sounds.         CSharp4_Db4,    272.14  },
            {   Sounds.         D4, 288.33  },
            {   Sounds.         DSharp4_Eb4,    305.47  },
            {   Sounds.         E4, 323.63  },
            {   Sounds.         F4, 342.88  },
            {   Sounds.         FSharp4_Gb4,    363.27  },
            {   Sounds.         G4, 384.87  },
            {   Sounds.         GSharp4_Ab4,    407.75  },
            {   Sounds.         A4, 432.00  },
            {   Sounds.         ASharp4_Bb4,    457.69  },
            {   Sounds.         B4, 484.90  },
            {   Sounds.         C5, 513.74  },
            {   Sounds.         CSharp5_Db5,    544.29  },
            {   Sounds.         D5, 576.65  },
            {   Sounds.         DSharp5_Eb5,    610.94  },
            {   Sounds.         E5, 647.27  },
            {   Sounds.         F5, 685.76  },
            {   Sounds.         FSharp5_Gb5,    726.53  },
            {   Sounds.         G5, 769.74  },
            {   Sounds.         GSharp5_Ab5,    815.51  },
            {   Sounds.         A5, 864.00  },
            {   Sounds.         ASharp5_Bb5,    915.38  },
            {   Sounds.         B5, 969.81  },
            {   Sounds.         C6, 1027.47 },
            {   Sounds.         CSharp6_Db6,    1088.57 },
            {   Sounds.         D6, 1153.30 },
            {   Sounds.         DSharp6_Eb6,    1221.88 },
            {   Sounds.         E6, 1294.54 },
            {   Sounds.         F6, 1371.51 }


        };
        public static Dictionary<Sounds, double> Sounds_434Hz = new Dictionary<Sounds, double>()
        {
            {   Sounds. D2, 72.42   },
{   Sounds.         DSharp2_Eb2,    76.72   },
{   Sounds.         E2, 81.28   },
{   Sounds.         F2, 86.12   },
{   Sounds.         FSharp2_Gb2,    91.24   },
{   Sounds.         G2, 96.66   },
{   Sounds.         GSharp2_Ab2,    102.41  },
{   Sounds.         A2, 108.50  },
{   Sounds.         ASharp2_Bb2,    114.95  },
{   Sounds.         B2, 121.79  },
{   Sounds.         C3, 129.03  },
{   Sounds.         CSharp3_Db3,    136.70  },
{   Sounds.         D3, 144.83  },
{   Sounds.         DSharp3_Eb3,    153.44  },
{   Sounds.         E3, 162.57  },
{   Sounds.         F3, 172.23  },
{   Sounds.         FSharp3_Gb3,    182.47  },
{   Sounds.         G3, 193.32  },
{   Sounds.         GSharp3_Ab3,    204.82  },
{   Sounds.         A3, 217.00  },
{   Sounds.         ASharp3_Bb3,    229.90  },
{   Sounds.         B3, 243.57  },
{   Sounds.         C4, 258.06  },
{   Sounds.         CSharp4_Db4,    273.40  },
{   Sounds.         D4, 289.66  },
{   Sounds.         DSharp4_Eb4,    306.88  },
{   Sounds.         E4, 325.13  },
{   Sounds.         F4, 344.47  },
{   Sounds.         FSharp4_Gb4,    364.95  },
{   Sounds.         G4, 386.65  },
{   Sounds.         GSharp4_Ab4,    409.64  },
{   Sounds.         A4, 434.00  },
{   Sounds.         ASharp4_Bb4,    459.81  },
{   Sounds.         B4, 487.15  },
{   Sounds.         C5, 516.12  },
{   Sounds.         CSharp5_Db5,    546.81  },
{   Sounds.         D5, 579.32  },
{   Sounds.         DSharp5_Eb5,    613.77  },
{   Sounds.         E5, 650.27  },
{   Sounds.         F5, 688.93  },
{   Sounds.         FSharp5_Gb5,    729.90  },
{   Sounds.         G5, 773.30  },
{   Sounds.         GSharp5_Ab5,    819.28  },
{   Sounds.         A5, 868.00  },
{   Sounds.         ASharp5_Bb5,    919.61  },
{   Sounds.         B5, 974.30  },
{   Sounds.         C6, 1032.23 },
{   Sounds.         CSharp6_Db6,    1093.61 },
{   Sounds.         D6, 1158.64 },
{   Sounds.         DSharp6_Eb6,    1227.54 },
{   Sounds.         E6, 1300.53 },
{   Sounds.         F6, 1377.86 }

        };
        public static Dictionary<Sounds, double> Sounds_436Hz = new Dictionary<Sounds, double>()
        {
            {   Sounds. D2, 72.75   },
{   Sounds.         DSharp2_Eb2,    77.07   },
{   Sounds.         E2, 81.66   },
{   Sounds.         F2, 86.51   },
{   Sounds.         FSharp2_Gb2,    91.66   },
{   Sounds.         G2, 97.11   },
{   Sounds.         GSharp2_Ab2,    102.88  },
{   Sounds.         A2, 109.00  },
{   Sounds.         ASharp2_Bb2,    115.48  },
{   Sounds.         B2, 122.35  },
{   Sounds.         C3, 129.62  },
{   Sounds.         CSharp3_Db3,    137.33  },
{   Sounds.         D3, 145.50  },
{   Sounds.         DSharp3_Eb3,    154.15  },
{   Sounds.         E3, 163.32  },
{   Sounds.         F3, 173.03  },
{   Sounds.         FSharp3_Gb3,    183.32  },
{   Sounds.         G3, 194.22  },
{   Sounds.         GSharp3_Ab3,    205.76  },
{   Sounds.         A3, 218.00  },
{   Sounds.         ASharp3_Bb3,    230.96  },
{   Sounds.         B3, 244.70  },
{   Sounds.         C4, 259.25  },
{   Sounds.         CSharp4_Db4,    274.66  },
{   Sounds.         D4, 290.99  },
{   Sounds.         DSharp4_Eb4,    308.30  },
{   Sounds.         E4, 326.63  },
{   Sounds.         F4, 346.05  },
{   Sounds.         FSharp4_Gb4,    366.63  },
{   Sounds.         G4, 388.43  },
{   Sounds.         GSharp4_Ab4,    411.53  },
{   Sounds.         A4, 436.00  },
{   Sounds.         ASharp4_Bb4,    461.93  },
{   Sounds.         B4, 489.39  },
{   Sounds.         C5, 518.49  },
{   Sounds.         CSharp5_Db5,    549.33  },
{   Sounds.         D5, 581.99  },
{   Sounds.         DSharp5_Eb5,    616.60  },
{   Sounds.         E5, 653.26  },
{   Sounds.         F5, 692.11  },
{   Sounds.         FSharp5_Gb5,    733.26  },
{   Sounds.         G5, 776.86  },
{   Sounds.         GSharp5_Ab5,    823.06  },
{   Sounds.         A5, 872.00  },
{   Sounds.         ASharp5_Bb5,    923.85  },
{   Sounds.         B5, 978.79  },
{   Sounds.         C6, 1036.99 },
{   Sounds.         CSharp6_Db6,    1098.65 },
{   Sounds.         D6, 1163.98 },
{   Sounds.         DSharp6_Eb6,    1233.19 },
{   Sounds.         E6, 1306.52 },
{   Sounds.         F6, 1384.21 }

        };
        public static Dictionary<Sounds, double> Sounds_438Hz = new Dictionary<Sounds, double>()
        {
            {   Sounds. D2, 73.08   },
{   Sounds.         DSharp2_Eb2,    77.43   },
{   Sounds.         E2, 82.03   },
{   Sounds.         F2, 86.91   },
{   Sounds.         FSharp2_Gb2,    92.08   },
{   Sounds.         G2, 97.55   },
{   Sounds.         GSharp2_Ab2,    103.35  },
{   Sounds.         A2, 109.50  },
{   Sounds.         ASharp2_Bb2,    116.01  },
{   Sounds.         B2, 122.91  },
{   Sounds.         C3, 130.22  },
{   Sounds.         CSharp3_Db3,    137.96  },
{   Sounds.         D3, 146.16  },
{   Sounds.         DSharp3_Eb3,    154.86  },
{   Sounds.         E3, 164.06  },
{   Sounds.         F3, 173.82  },
{   Sounds.         FSharp3_Gb3,    184.16  },
{   Sounds.         G3, 195.11  },
{   Sounds.         GSharp3_Ab3,    206.71  },
{   Sounds.         A3, 219.00  },
{   Sounds.         ASharp3_Bb3,    232.02  },
{   Sounds.         B3, 245.82  },
{   Sounds.         C4, 260.44  },
{   Sounds.         CSharp4_Db4,    275.92  },
{   Sounds.         D4, 292.33  },
{   Sounds.         DSharp4_Eb4,    309.71  },
{   Sounds.         E4, 328.13  },
{   Sounds.         F4, 347.64  },
{   Sounds.         FSharp4_Gb4,    368.31  },
{   Sounds.         G4, 390.21  },
{   Sounds.         GSharp4_Ab4,    413.42  },
{   Sounds.         A4, 438.00  },
{   Sounds.         ASharp4_Bb4,    464.04  },
{   Sounds.         B4, 491.64  },
{   Sounds.         C5, 520.87  },
{   Sounds.         CSharp5_Db5,    551.85  },
{   Sounds.         D5, 584.66  },
{   Sounds.         DSharp5_Eb5,    619.43  },
{   Sounds.         E5, 656.26  },
{   Sounds.         F5, 695.28  },
{   Sounds.         FSharp5_Gb5,    736.63  },
{   Sounds.         G5, 780.43  },
{   Sounds.         GSharp5_Ab5,    826.83  },
{   Sounds.         A5, 876.00  },
{   Sounds.         ASharp5_Bb5,    928.09  },
{   Sounds.         B5, 983.28  },
{   Sounds.         C6, 1041.74 },
{   Sounds.         CSharp6_Db6,    1103.69 },
{   Sounds.         D6, 1169.32 },
{   Sounds.         DSharp6_Eb6,    1238.85 },
{   Sounds.         E6, 1312.52 },
{   Sounds.         F6, 1390.56 }

        };
        public static Dictionary<Sounds, double> Sounds_440Hz = new Dictionary<Sounds, double>()
        {
            {   Sounds. D2, 73.42   },
{   Sounds.         DSharp2_Eb2,    77.78   },
{   Sounds.         E2, 82.41   },
{   Sounds.         F2, 87.31   },
{   Sounds.         FSharp2_Gb2,    92.50   },
{   Sounds.         G2, 98.00   },
{   Sounds.         GSharp2_Ab2,    103.83  },
{   Sounds.         A2, 110.00  },
{   Sounds.         ASharp2_Bb2,    116.54  },
{   Sounds.         B2, 123.47  },
{   Sounds.         C3, 130.81  },
{   Sounds.         CSharp3_Db3,    138.59  },
{   Sounds.         D3, 146.83  },
{   Sounds.         DSharp3_Eb3,    155.56  },
{   Sounds.         E3, 164.81  },
{   Sounds.         F3, 174.61  },
{   Sounds.         FSharp3_Gb3,    185.00  },
{   Sounds.         G3, 196.00  },
{   Sounds.         GSharp3_Ab3,    207.65  },
{   Sounds.         A3, 220.00  },
{   Sounds.         ASharp3_Bb3,    233.08  },
{   Sounds.         B3, 246.94  },
{   Sounds.         C4, 261.63  },
{   Sounds.         CSharp4_Db4,    277.18  },
{   Sounds.         D4, 293.66  },
{   Sounds.         DSharp4_Eb4,    311.13  },
{   Sounds.         E4, 329.63  },
{   Sounds.         F4, 349.23  },
{   Sounds.         FSharp4_Gb4,    369.99  },
{   Sounds.         G4, 392.00  },
{   Sounds.         GSharp4_Ab4,    415.30  },
{   Sounds.         A4, 440.00  },
{   Sounds.         ASharp4_Bb4,    466.16  },
{   Sounds.         B4, 493.88  },
{   Sounds.         C5, 523.25  },
{   Sounds.         CSharp5_Db5,    554.37  },
{   Sounds.         D5, 587.33  },
{   Sounds.         DSharp5_Eb5,    622.25  },
{   Sounds.         E5, 659.25  },
{   Sounds.         F5, 698.46  },
{   Sounds.         FSharp5_Gb5,    739.99  },
{   Sounds.         G5, 783.99  },
{   Sounds.         GSharp5_Ab5,    830.61  },
{   Sounds.         A5, 880.00  },
{   Sounds.         ASharp5_Bb5,    932.33  },
{   Sounds.         B5, 987.77  },
{   Sounds.         C6, 1046.50 },
{   Sounds.         CSharp6_Db6,    1108.73 },
{   Sounds.         D6, 1174.66 },
{   Sounds.         DSharp6_Eb6,    1244.51 },
{   Sounds.         E6, 1318.51 },
{   Sounds.         F6, 1396.91 }

        };
        public static Dictionary<Sounds, double> Sounds_442Hz = new Dictionary<Sounds, double>()
        {
            {   Sounds. D2, 73.75   },
{   Sounds.         DSharp2_Eb2,    78.14   },
{   Sounds.         E2, 82.78   },
{   Sounds.         F2, 87.70   },
{   Sounds.         FSharp2_Gb2,    92.92   },
{   Sounds.         G2, 98.44   },
{   Sounds.         GSharp2_Ab2,    104.30  },
{   Sounds.         A2, 110.50  },
{   Sounds.         ASharp2_Bb2,    117.07  },
{   Sounds.         B2, 124.03  },
{   Sounds.         C3, 131.41  },
{   Sounds.         CSharp3_Db3,    139.22  },
{   Sounds.         D3, 147.50  },
{   Sounds.         DSharp3_Eb3,    156.27  },
{   Sounds.         E3, 165.56  },
{   Sounds.         F3, 175.41  },
{   Sounds.         FSharp3_Gb3,    185.84  },
{   Sounds.         G3, 196.89  },
{   Sounds.         GSharp3_Ab3,    208.60  },
{   Sounds.         A3, 221.00  },
{   Sounds.         ASharp3_Bb3,    234.14  },
{   Sounds.         B3, 248.06  },
{   Sounds.         C4, 262.81  },
{   Sounds.         CSharp4_Db4,    278.44  },
{   Sounds.         D4, 295.00  },
{   Sounds.         DSharp4_Eb4,    312.54  },
{   Sounds.         E4, 331.13  },
{   Sounds.         F4, 350.82  },
{   Sounds.         FSharp4_Gb4,    371.68  },
{   Sounds.         G4, 393.78  },
{   Sounds.         GSharp4_Ab4,    417.19  },
{   Sounds.         A4, 442.00  },
{   Sounds.         ASharp4_Bb4,    468.28  },
{   Sounds.         B4, 496.13  },
{   Sounds.         C5, 525.63  },
{   Sounds.         CSharp5_Db5,    556.88  },
{   Sounds.         D5, 590.00  },
{   Sounds.         DSharp5_Eb5,    625.08  },
{   Sounds.         E5, 662.25  },
{   Sounds.         F5, 701.63  },
{   Sounds.         FSharp5_Gb5,    743.35  },
{   Sounds.         G5, 787.55  },
{   Sounds.         GSharp5_Ab5,    834.38  },
{   Sounds.         A5, 884.00  },
{   Sounds.         ASharp5_Bb5,    936.57  },
{   Sounds.         B5, 992.26  },
{   Sounds.         C6, 1051.26 },
{   Sounds.         CSharp6_Db6,    1113.77 },
{   Sounds.         D6, 1180.00 },
{   Sounds.         DSharp6_Eb6,    1250.16 },
{   Sounds.         E6, 1324.50 },
{   Sounds.         F6, 1403.26 }

        };
        public static Dictionary<Sounds, double> Sounds_444Hz = new Dictionary<Sounds, double>()
        {
            {   Sounds. D2, 74.08   },
{   Sounds.         DSharp2_Eb2,    78.49   },
{   Sounds.         E2, 83.16   },
{   Sounds.         F2, 88.10   },
{   Sounds.         FSharp2_Gb2,    93.34   },
{   Sounds.         G2, 98.89   },
{   Sounds.         GSharp2_Ab2,    104.77  },
{   Sounds.         A2, 111.00  },
{   Sounds.         ASharp2_Bb2,    117.60  },
{   Sounds.         B2, 124.59  },
{   Sounds.         C3, 132.00  },
{   Sounds.         CSharp3_Db3,    139.85  },
{   Sounds.         D3, 148.17  },
{   Sounds.         DSharp3_Eb3,    156.98  },
{   Sounds.         E3, 166.31  },
{   Sounds.         F3, 176.20  },
{   Sounds.         FSharp3_Gb3,    186.68  },
{   Sounds.         G3, 197.78  },
{   Sounds.         GSharp3_Ab3,    209.54  },
{   Sounds.         A3, 222.00  },
{   Sounds.         ASharp3_Bb3,    235.20  },
{   Sounds.         B3, 249.19  },
{   Sounds.         C4, 264.00  },
{   Sounds.         CSharp4_Db4,    279.70  },
{   Sounds.         D4, 296.33  },
{   Sounds.         DSharp4_Eb4,    313.96  },
{   Sounds.         E4, 332.62  },
{   Sounds.         F4, 352.40  },
{   Sounds.         FSharp4_Gb4,    373.36  },
{   Sounds.         G4, 395.56  },
{   Sounds.         GSharp4_Ab4,    419.08  },
{   Sounds.         A4, 444.00  },
{   Sounds.         ASharp4_Bb4,    470.40  },
{   Sounds.         B4, 498.37  },
{   Sounds.         C5, 528.01  },
{   Sounds.         CSharp5_Db5,    559.40  },
{   Sounds.         D5, 592.67  },
{   Sounds.         DSharp5_Eb5,    627.91  },
{   Sounds.         E5, 665.25  },
{   Sounds.         F5, 704.81  },
{   Sounds.         FSharp5_Gb5,    746.72  },
{   Sounds.         G5, 791.12  },
{   Sounds.         GSharp5_Ab5,    838.16  },
{   Sounds.         A5, 888.00  },
{   Sounds.         ASharp5_Bb5,    940.80  },
{   Sounds.         B5, 996.75  },
{   Sounds.         C6, 1056.02 },
{   Sounds.         CSharp6_Db6,    1118.81 },
{   Sounds.         D6, 1185.34 },
{   Sounds.         DSharp6_Eb6,    1255.82 },
{   Sounds.         E6, 1330.50 },
{   Sounds.         F6, 1409.61 }

        };
        public static Dictionary<Sounds, double> Sounds_446Hz = new Dictionary<Sounds, double>()
        {
            {   Sounds. D2, 74.42   },
{   Sounds.         DSharp2_Eb2,    78.84   },
{   Sounds.         E2, 83.53   },
{   Sounds.         F2, 88.50   },
{   Sounds.         FSharp2_Gb2,    93.76   },
{   Sounds.         G2, 99.34   },
{   Sounds.         GSharp2_Ab2,    105.24  },
{   Sounds.         A2, 111.50  },
{   Sounds.         ASharp2_Bb2,    118.13  },
{   Sounds.         B2, 125.15  },
{   Sounds.         C3, 132.60  },
{   Sounds.         CSharp3_Db3,    140.48  },
{   Sounds.         D3, 148.83  },
{   Sounds.         DSharp3_Eb3,    157.68  },
{   Sounds.         E3, 167.06  },
{   Sounds.         F3, 177.00  },
{   Sounds.         FSharp3_Gb3,    187.52  },
{   Sounds.         G3, 198.67  },
{   Sounds.         GSharp3_Ab3,    210.48  },
{   Sounds.         A3, 223.00  },
{   Sounds.         ASharp3_Bb3,    236.26  },
{   Sounds.         B3, 250.31  },
{   Sounds.         C4, 265.19  },
{   Sounds.         CSharp4_Db4,    280.96  },
{   Sounds.         D4, 297.67  },
{   Sounds.         DSharp4_Eb4,    315.37  },
{   Sounds.         E4, 334.12  },
{   Sounds.         F4, 353.99  },
{   Sounds.         FSharp4_Gb4,    375.04  },
{   Sounds.         G4, 397.34  },
{   Sounds.         GSharp4_Ab4,    420.97  },
{   Sounds.         A4, 446.00  },
{   Sounds.         ASharp4_Bb4,    472.52  },
{   Sounds.         B4, 500.62  },
{   Sounds.         C5, 530.39  },
{   Sounds.         CSharp5_Db5,    561.92  },
{   Sounds.         D5, 595.34  },
{   Sounds.         DSharp5_Eb5,    630.74  },
{   Sounds.         E5, 668.24  },
{   Sounds.         F5, 707.98  },
{   Sounds.         FSharp5_Gb5,    750.08  },
{   Sounds.         G5, 794.68  },
{   Sounds.         GSharp5_Ab5,    841.94  },
{   Sounds.         A5, 892.00  },
{   Sounds.         ASharp5_Bb5,    945.04  },
{   Sounds.         B5, 1001.24 },
{   Sounds.         C6, 1060.77 },
{   Sounds.         CSharp6_Db6,    1123.85 },
{   Sounds.         D6, 1190.68 },
{   Sounds.         DSharp6_Eb6,    1261.48 },
{   Sounds.         E6, 1336.49 },
{   Sounds.         F6, 1415.96 }

        };

        public static string GetSoundName(this Sounds ss)
        {
            string s = "";

            switch (ss)
            {
                case Sounds.D2: { s = "D2"; break; }
                case Sounds.DSharp2_Eb2: { s = "D2#/E2b"; break; }
                case Sounds.E2: { s = "E2"; break; }
                case Sounds.F2: { s = "F2"; break; }
                case Sounds.FSharp2_Gb2: { s = "F2#/G2b"; break; }
                case Sounds.G2: { s = "G2"; break; }
                case Sounds.GSharp2_Ab2: { s = "G2#/A2b"; break; }
                case Sounds.A2: { s = "A2"; break; }
                case Sounds.ASharp2_Bb2: { s = "A2#/B2b"; break; }
                case Sounds.B2: { s = "B2"; break; }
                case Sounds.C3: { s = "C3"; break; }
                case Sounds.CSharp3_Db3: { s = "C3#/D3b"; break; }
                case Sounds.D3: { s = "D3"; break; }
                case Sounds.DSharp3_Eb3: { s = "D3#/E3b"; break; }
                case Sounds.E3: { s = "E3"; break; }
                case Sounds.F3: { s = "F3"; break; }
                case Sounds.FSharp3_Gb3: { s = "F3#/G3b"; break; }
                case Sounds.G3: { s = "G3"; break; }
                case Sounds.GSharp3_Ab3: { s = "G3#/A3b"; break; }
                case Sounds.A3: { s = "A3"; break; }
                case Sounds.ASharp3_Bb3: { s = "A3#/B3b"; break; }
                case Sounds.B3: { s = "B3"; break; }
                case Sounds.C4: { s = "C4"; break; }
                case Sounds.CSharp4_Db4: { s = "C4#/D4b"; break; }
                case Sounds.D4: { s = "D4"; break; }
                case Sounds.DSharp4_Eb4: { s = "D4#/E4b"; break; }
                case Sounds.E4: { s = "E4"; break; }
                case Sounds.F4: { s = "F4"; break; }
                case Sounds.FSharp4_Gb4: { s = "F4#/G4b"; break; }
                case Sounds.G4: { s = "G4"; break; }
                case Sounds.GSharp4_Ab4: { s = "G4#/A4b"; break; }
                case Sounds.A4: { s = "A4"; break; }
                case Sounds.ASharp4_Bb4: { s = "A4#/B4b"; break; }
                case Sounds.B4: { s = "B4"; break; }
                case Sounds.C5: { s = "C5"; break; }
                case Sounds.CSharp5_Db5: { s = "C5#/D5b"; break; }
                case Sounds.D5: { s = "D5"; break; }
                case Sounds.DSharp5_Eb5: { s = "D5#/E5b"; break; }
                case Sounds.E5: { s = "E5"; break; }
                case Sounds.F5: { s = "F5"; break; }
                case Sounds.FSharp5_Gb5: { s = "F5#/G5b"; break; }
                case Sounds.G5: { s = "G5"; break; }
                case Sounds.GSharp5_Ab5: { s = "G5#/A5b"; break; }
                case Sounds.A5: { s = "A5"; break; }
                case Sounds.ASharp5_Bb5: { s = "A5#/B5b"; break; }
                case Sounds.B5: { s = "B5"; break; }
                case Sounds.C6: { s = "C6"; break; }
                case Sounds.CSharp6_Db6: { s = "C6#/D6b"; break; }
                case Sounds.D6: { s = "D6"; break; }
                case Sounds.DSharp6_Eb6: { s = "D6#/E6b"; break; }
                case Sounds.E6: { s = "E6"; break; }
                case Sounds.F6: { s = "F6"; break; }
            }

            return SubscriptChars.ConvertToSubscript(s);
        }
        public static Dictionary<Sounds, double> GetSoundsList(Tuning t)
        {
            switch (t)
            {
                case Tuning.T432Hz:
                    return Sounds_432Hz;
                case Tuning.T434Hz:
                    return Sounds_434Hz;
                case Tuning.T436Hz:
                    return Sounds_436Hz;
                case Tuning.T438Hz:
                    return Sounds_438Hz;
                case Tuning.T440Hz:
                    return Sounds_440Hz;
                case Tuning.T442Hz:
                    return Sounds_442Hz;
                case Tuning.T444Hz:
                    return Sounds_444Hz;
                case Tuning.T446Hz:
                    return Sounds_446Hz;
                default:
                    return Sounds_440Hz;
            }
        }

        /// <summary>
        /// Item1 to granica dźwięku od lewej. Item2 to granica dźwięku od prawej
        /// </summary>
        /// <param name="t"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Tuple<double, double> GetSoundBoundaries(Tuning t, Sounds s)
        {
            var d = GetSoundsList(t);
            int soundIndex = (int)s;
            double sf = d[s];

            double? bf = null;
            double? af = null;

            if (soundIndex > 0)
            {
                Sounds soundBefore = (Sounds)(soundIndex - 1);
                bf = (sf - d[soundBefore]) / 2.0;
            }
            if (soundIndex < d.Count)
            {
                Sounds soundAfter = (Sounds)(soundIndex + 1);
                af = (d[soundAfter] - sf) / 2.0;
            }

            if (bf == null)
                return new Tuple<double, double>(sf - af.Value, sf + af.Value);
            if (af == null)
                return new Tuple<double, double>(sf - bf.Value, sf + bf.Value);

            return new Tuple<double, double>(sf - bf.Value, sf + af.Value);
        }

        public static Sounds? RecognizeSound(double freq, Tuning t)
        {
            var d = GetSoundsList(t);
            double distance = System.Math.Abs(d[Sounds.D2] - freq);
            Sounds tr = Sounds.D2;

            for (int i = 1; i < d.Count; i++)
            {
                Sounds cs = (Sounds)(i);
                double dist = System.Math.Abs(d[cs] - freq);
                if (dist < distance)
                {
                    distance = dist;
                    tr = cs;
                }
                else
                {
                    break;
                }
            }
            return tr;
        }
        public static double GetSoundFrequency(this Sounds s, Tuning t)
        {
            var d = GetSoundsList(t);
            return d[s];
        }

        public static string GetTuningName(this Tuning t)
        {
            switch (t)
            {
                case Tuning.T432Hz:
                    return "432Hz";
                case Tuning.T434Hz:
                    return "434Hz";
                case Tuning.T436Hz:
                    return "436Hz";
                case Tuning.T438Hz:
                    return "438";
                case Tuning.T440Hz:
                    return "440Hz";
                case Tuning.T442Hz:
                    return "442Hz";
                case Tuning.T444Hz:
                    return "444Hz";
                case Tuning.T446Hz:
                    return "446Hz";
                default:
                    return "";
            }
        }

        /// <summary>
        /// Zawiera w sobie progi od 0 do 4 kolejnych strun. 0 - struna pierwsza, 5 - struna ostatnia.
        /// UWAGA! Struna o indeksie 2 (trzecia) ma tylko cztery dźwięki (typowe dla gitar)
        /// </summary>
        public static Sounds[][] GuitarStringBaseSoudns = {
            new Sounds[]{Sounds.E4, Sounds.F4, Sounds.FSharp4_Gb4, Sounds.G4, Sounds.GSharp4_Ab4 },
            new Sounds[]{Sounds.B3, Sounds.C4, Sounds.CSharp4_Db4, Sounds.D4, Sounds.DSharp4_Eb4 },
            new Sounds[]{Sounds.G3, Sounds.GSharp3_Ab3, Sounds.A3, Sounds.ASharp3_Bb3 },
            new Sounds[]{Sounds.D3, Sounds.DSharp3_Eb3, Sounds.E3, Sounds.F3, Sounds.FSharp3_Gb3},
            new Sounds[]{Sounds.A2, Sounds.ASharp2_Bb2, Sounds.B2, Sounds.C3, Sounds.CSharp3_Db3},
            new Sounds[]{Sounds.E2, Sounds.F2, Sounds.FSharp2_Gb2, Sounds.G2, Sounds.GSharp2_Ab2}
        };
    }

    public enum Sounds
    {
        D2,
        DSharp2_Eb2,
        E2,
        F2,
        FSharp2_Gb2,
        G2,
        GSharp2_Ab2,
        A2,
        ASharp2_Bb2,
        B2,
        C3,
        CSharp3_Db3,
        D3,
        DSharp3_Eb3,
        E3,
        F3,
        FSharp3_Gb3,
        G3,
        GSharp3_Ab3,
        A3,
        ASharp3_Bb3,
        B3,
        C4,
        CSharp4_Db4,
        D4,
        DSharp4_Eb4,
        E4,
        F4,
        FSharp4_Gb4,
        G4,
        GSharp4_Ab4,
        A4,
        ASharp4_Bb4,
        B4,
        C5,
        CSharp5_Db5,
        D5,
        DSharp5_Eb5,
        E5,
        F5,
        FSharp5_Gb5,
        G5,
        GSharp5_Ab5,
        A5,
        ASharp5_Bb5,
        B5,
        C6,
        CSharp6_Db6,
        D6,
        DSharp6_Eb6,
        E6,
        F6
    };
    public enum Tuning
    {
        T432Hz,
        T434Hz,
        T436Hz,
        T438Hz,
        T440Hz,
        T442Hz,
        T444Hz,
        T446Hz
    }
}
