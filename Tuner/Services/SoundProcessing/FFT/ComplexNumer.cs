﻿using System;
namespace Tuner.Services.SoundProcessing.FFT
{
    /// <summary>
    /// Klasa reprezentująca liczbę zespoloną
    /// </summary>
    public class ComplexNumber
    {
        /// <summary>
        /// Wartość rzeczywista liczby zespolonej
        /// </summary>
        public double real;
        /// <summary>
        /// Wartość urojona liczby zespolonej
        /// </summary>
        public double imaginary;
        /// <summary>
        /// Moduł liczby zespolonej. W celu ograniczenia zajmowania pamięci, wartość modułu nie jest przechowywana, lecz w czasie pobierania obliczana z wartości rzeczywistej i urojonej liczby zespolonej
        /// </summary>
        public double module
        {
            get
            {
                return Math.Sqrt(real * real + imaginary * imaginary);
            }
        }
        /// <summary>
        /// Przeciążenie operatora * do wykonywania mnożenia na liczbach zespolonych
        /// </summary>
        /// <param name="a">Czynnik zespolony mnożenia</param>
        /// <param name="b">Czynnik zespolony mnożenia</param>
        /// <returns></returns>
        public static ComplexNumber operator *(ComplexNumber a, ComplexNumber b)
        {
            ComplexNumber c = new ComplexNumber();
            c.real = a.real * b.real - a.imaginary * b.imaginary;
            c.imaginary = a.imaginary * b.real + a.real * b.imaginary;
            return c;
        }
        /// <summary>
        /// Przeciążenie operatora + do wykonywania dodawania na liczbach zespolonych
        /// </summary>
        /// <param name="a">Składnik zespolony dodawania</param>
        /// <param name="b">Składnik zespolony dodawania</param>
        /// <returns></returns>
        public static ComplexNumber operator +(ComplexNumber a, ComplexNumber b)
        {
            ComplexNumber c = new ComplexNumber();
            c.real = a.real + b.real;
            c.imaginary = a.imaginary + b.imaginary;
            return c;
        }
        /// <summary>
        /// Przeciążenie operatora - do wykonywania odejmowania na liczbach zespolonych
        /// </summary>
        /// <param name="a">Odjemna zespolona odejmowania</param>
        /// <param name="b">Odjemnik zespolony odejmowania</param>
        /// <returns></returns>
        public static ComplexNumber operator -(ComplexNumber a, ComplexNumber b)
        {
            ComplexNumber c = new ComplexNumber();
            c.real = a.real - b.real;
            c.imaginary = a.imaginary - b.imaginary;
            return c;
        }

    }
}
