﻿using System;
using System.Linq;

namespace Tuner.Services.SoundProcessing.FFT
{
    public static class FastFourierTransformAlgorithm
    {
        public static FFTPoint[] CalculateFFT(FFTPoint[] data, double sampleRate)
        {
            ComplexNumber[] numbers = new ComplexNumber[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                numbers[i] = new ComplexNumber()
                {
                    real = data[i].Y,
                    imaginary = 0
                };
            }
            return CalculateFFT(numbers, sampleRate);
        }

        public static FFTPoint[] CalculateFFT(double[] data, double sampleRate)
        {
            ComplexNumber[] numbers = new ComplexNumber[data.Length];
            for (int i = 0; i < data.Length; i++)
            {
                numbers[i] = new ComplexNumber()
                {
                    real = data[i],
                    imaginary = 0
                };
            }
            return CalculateFFT(numbers, sampleRate);
        }

        /// <summary>
        /// Metoda, która przeprowadza Transformatę Fouriera na tabeli liczb zespolonych. Jest ona implementacją algorytmu Cooleya-Tukeya
        /// </summary>
        /// <param name="data">Tabela liczb zespolonych. Każda liczba z tej tabeli powinna mieć wartość rzeczywistą równą wartości odpowiadającej jej próbki i wartość urojoną równą zero. Ilość próbek w tabeli musi być całkowitą potęgą liczby 2</param>
        /// <param name="sampleRate">Szybkość z jaką plik Wave był nagrywany. Jest to ilość próbek w jednej sekundzie nagrania</param>
        /// <returns></returns>
        public static FFTPoint[] CalculateFFT(ComplexNumber[] data, double sampleRate)
        {
            if (data.Length < 2)
                return new FFTPoint[] { new FFTPoint(), new FFTPoint() };
            if (data.Length == 0)
                return new FFTPoint[1] { new FFTPoint() { Decibels = 0, Frequency = 0, Magnitude = 0 } };
            data = AdjustTableSize(data);
            if (data.Length == 0)
                return new FFTPoint[1] { new FFTPoint() { Decibels = 0, Frequency = 0, Magnitude = 0 } };
            data = Divide(data, false);
            var points = ToFFTPointArray(data, sampleRate);
            return points;
        }

        public static ComplexNumber[] CalculateFFT(double[] data)
        {
            var numbers = prepare_data(data);
            numbers = Divide(numbers, false);
            return numbers;
        }

        public static FFTPoint[] ToFFTPointArray(ComplexNumber[] data, double sampleRate)
        {
            var power = data.Sum(x => x.module);
            FFTPoint[] points = new FFTPoint[data.Length]; //zostaje utworzona tabela wyników Transformacji Fouriera
            for (int i = 0; i < points.Length; i++)
            {
                points[i] = new FFTPoint(((double)i / (double)points.Length * sampleRate * 2), data[i].module / points.Length * 2, power); //Tworzenie punktu wynikowego i przypisanie go do tabeli wynikowej. Pierwszy parametr to częstotliwość i jest on wynikiem mnożenia indeksu tabeli razy ilość próbek w ciągu sekundy i dzielenia przez ilość próbek. Drugi parametr to magnituda, która jest modułem liczby zespolonej podzielonej przez ilość próbek w celach normalizacyjnych
            }
            return points;
        }

        public static ComplexNumber[] CalculateReverseFFT(FFTPoint[] data, double sampleRate)
        {
            ComplexNumber[] numbers = new ComplexNumber[data.Length];
            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i] = new ComplexNumber();
                numbers[i].real = data[i].Magnitude;
                numbers[i].imaginary = 0;
            }
            numbers = Divide(numbers, true);
            double rootOfLength = Math.Sqrt(data.Length);
            for (int i = 0; i < numbers.Length; i++)
            {
                numbers[i].real = numbers[i].real / rootOfLength;
                numbers[i].imaginary = numbers[i].imaginary / rootOfLength;
            }
            return numbers;
        }

        public static double[] CalculateReverseFFT(ComplexNumber[] data)
        {
            //copying
            var numbers = new ComplexNumber[data.Length];
            for (int i = 0; i < data.Length; i++)
                numbers[i] = new ComplexNumber()
                {
                    real = data[i].real,
                    imaginary = data[i].imaginary
                };
            //performing
            numbers = Divide(numbers, true);
            double length = (double)numbers.Length;
            double[] probes = new double[numbers.Length];
            for (int i = 0; i < numbers.Length; i++)
            {
                probes[i] = numbers[i].real / length;
            }
            return probes;
        }


        /// <summary>
        /// Metoda, która dzieli tabelę liczb zespolonych na dwie. Pierwsza tabela zawiera w sobie próbki parzyste, a druga nieparzyste. Dzielenie będzie się odbywać rekurencyjnie, aż do momentu, gdy w każdej tabeli będzie tylko jedna próbka. Wtedy następuje obliczenie wartości dla próbek i sklejenie tabel
        /// </summary>
        /// <param name="data">Tablica liczb zespolonych, która zostanie podzielona</param>
        /// <returns></returns>
        private static ComplexNumber[] Divide(ComplexNumber[] data, bool isReverse)
        {
            ComplexNumber[] t1 = new ComplexNumber[data.Length / 2];
            ComplexNumber[] t2 = new ComplexNumber[data.Length / 2];

            for (int i = 0; i < t1.Length; i++)
            {
                t1[i] = data[i * 2];
                t2[i] = data[i * 2 + 1];
            }
            if (t1.Length != 1)
            {
                t1 = Divide(t1, isReverse);
                t2 = Divide(t2, isReverse);
            }
            for (int i = 0; i < t1.Length; i++)
                Motylek(ref t1[i], ref t2[i], i, t1.Length + t2.Length, isReverse);
            return Merge(t1, t2);

        }
        /// <summary>
        /// Wykonanie obliczenia "motylkowego", przemnożenie liczby zespolonej xB przez e^(2 * pi * i * pozycja / ilość próbek), dodanie wartości mnożenia do próbki parzystej i odjęcie jej od próbki nieparzystej 
        /// </summary>
        /// <param name="xA">Liczba zespolona z tabeli próbek parzystych</param>
        /// <param name="xB">Liczba zespolona z tabeli próbek nieparzystych mająca w swojej tabeli ten sam indeks, co liczba xA</param>
        /// <param name="position">Pozycja liczby zespolonej w tabeli. Indeksowanie od zera.</param>
        /// <param name="collectionLength">Wielkość kolekcji, suma długości tabeli próbek parzystych i nieparzystych</param>
        private static void Motylek(ref ComplexNumber xA, ref ComplexNumber xB, int position, int collectionLength, bool isReverse)
        {
            ComplexNumber W = ConvertCoefficientWToComplex(isReverse ? position : (position * -1), collectionLength);
            ComplexNumber temp1 = xA + W * xB;
            ComplexNumber temp2 = xA - W * xB;
            xA.real = temp1.real;
            xA.imaginary = temp1.imaginary;
            xB.real = temp2.real;
            xB.imaginary = temp2.imaginary;

        }
        /// <summary>
        /// Wykonanie operacji e^(2 * pi * i * pozycja / ilość próbek). Podniesienie liczby Eulera do potęgi urojonej zostało przeprowadzone poprzez zamianę postaci wykładniczej na trygonometryczną
        /// </summary>
        /// <param name="k">Pozycja próbki w tabeli indeksowana od zera</param>
        /// <param name="N">Suma ilości próbek w tabeli próbek parzystych i nieparzystych</param>
        /// <returns></returns>
        private static ComplexNumber ConvertCoefficientWToComplex(int k, int N)
        {
            ComplexNumber W = new ComplexNumber();
            W.real = Math.Cos(2 * Math.PI * k / N);
            W.imaginary = Math.Sin(2 * Math.PI * k * -1 / N);
            return W;
        }
        /// <summary>
        /// Łączenie tabel próbek parzystych i nieparzystych w jedną tabelę. Realizowane jest ono w sposób następujący: najpierw wszystkie próbki z tabeli próbek parzystych, potem wszystkie z tabeli nieparzystych
        /// </summary>
        /// <param name="t1">Tabela próbek parzystych</param>
        /// <param name="t2">Tabela próbek nieparzystych</param>
        /// <returns></returns>
        private static ComplexNumber[] Merge(ComplexNumber[] t1, ComplexNumber[] t2)
        {
            ComplexNumber[] t3 = new ComplexNumber[t1.Length + t2.Length];
            for (int i = 0; i < t1.Length; i++)
                t3[i] = t1[i];
            for (int i = 0; i < t2.Length; i++)
            {
                t3[i + t1.Length] = t2[i];
            }
            return t3;
        }

        /// <summary>
        /// Ta metoda tworzy tabelę o wielkości będącej potęgą liczby dwa z tabeli próbek i dopełnia zerami
        /// </summary>
        /// <param name="probes"></param>
        /// <returns></returns>
        private static ComplexNumber[] AdjustTableSize(ComplexNumber[] probes)
        {
            double amount = probes.Length;
            int power = 0;
            while (amount > 1)
            {
                amount = amount / 2.0;
                power++;
            }
            double newLength = Math.Pow(2, power);
            ComplexNumber[] toReturn = new ComplexNumber[(int)newLength];
            for (int i = 0; i < probes.Length; i++)
            {
                toReturn[i] = probes[i];
            }
            for (int i = probes.Length; i < newLength; i++)
            {
                toReturn[i] = new ComplexNumber() { imaginary = 0, real = 0 };
            }
            return toReturn;
        }

        private static int get_smallest_power_of_two_greater_than(double value)
        {
            double amount = value;
            int power = 0;
            while (amount > 1)
            {
                amount = amount / 2.0;
                power++;
            }
            double newLength = Math.Pow(2, power);
            return (int)newLength;
        }

        private static ComplexNumber[] prepare_data(double[] data)
        {
            int length = get_smallest_power_of_two_greater_than(data.Length);
            ComplexNumber[] numbers = new ComplexNumber[length];
            for (int i = 0; i < data.Length; i++)
            {
                numbers[i] = new ComplexNumber()
                {
                    real = data[i],
                    imaginary = 0
                };
            }
            for (int i = data.Length; i < length; i++)
            {
                numbers[i] = new ComplexNumber() { real = 0, imaginary = 0 };
            }
            return numbers;
        }
    }
}
