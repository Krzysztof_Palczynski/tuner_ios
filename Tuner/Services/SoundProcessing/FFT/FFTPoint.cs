﻿using System;
namespace Tuner.Services.SoundProcessing.FFT
{
    /// <summary>
    /// Klasa reprezentująca próbkę danych po wykonaniu Transformaty Fouriera na danych z pliku Wave. Obiekty tej klasy są wykorzystywane do wykreślenia wykresu i do eksportu danych do pliku tekstowego
    /// </summary>
    public class FFTPoint
    {
        /// <summary>
        /// Częstotliwość, wartość na osi X
        /// </summary>
        public double Frequency { get; set; }
        /// <summary>
        /// Magnituda, modułl liczby zespolonej, składnik do obliczenia poziomu w decybelach
        /// </summary>
        public double Magnitude { get; set; }

        public double X { get { return Frequency; } }

        public double Y { get { return Magnitude; } }

        public double Decibels { get; set; }

        ///// <summary>
        ///// Poziom natężenia w decybelach. Jest on obliczany na podstawie magnitudy zgodnie ze wzorem U[dB] = 20log(Magnituda /32768). 32768 jest wartością stałą, ponieważ program przyjmuje tyle próbek do wykonania Transformaty Fouriera
        ///// </summary>
        //public double Decibles { get { return Magnitude == 0 ? -200 : 20 * Math.Log10(Magnitude / 32768.0); } set { Magnitude = Math.Pow(10, (value / 20)) * 32768; } }

        public FFTPoint()
        {
            Frequency = 0;
            Magnitude = 0;
        }

        public FFTPoint(double frequency, double magnitude) : this()
        {
            this.Frequency = frequency;
            this.Magnitude = magnitude;
            Decibels = -200;
        }

        public FFTPoint(double frequency, double magnitude, double energy) : this(frequency, magnitude)
        {
            Decibels = 20 * Math.Log10(Magnitude / energy);
        }

    }
}
