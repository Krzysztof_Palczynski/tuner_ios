﻿using System;
using System.Collections.Generic;
using System.Linq;
using Tuner.Services.Mathematics;
using Tuner.Services.SoundProcessing.FFT;

namespace Tuner.Services.SoundProcessing
{
    public static class SoundRecognizer
    {
        public static double FindAcousticResonanceFrequency(FFTPoint[] table, double tolerance, double minimumFrequency = 1)
        {
            //podstawa fizyczna algorytmu:
            //Idealny rezonans akustyczny jest szeregiem złożonym z częstotliwości
            //bazowej i kolejnych wyższych harmonicznych o częstotliwościach będących
            //całkowitą wielokrotnością częstotliwości bazowej

            //Celem tego algorytmu jest znalezienie częstotliwości bazowej

            if (table == null || table.Length == 0)
                return 0;

            int fc = table.Length / 100;
            if (fc < 10)
            {
                if (table.Length > 10)
                {
                    fc = 10;
                }
                else
                    fc = table.Length;
            }

            //filtrowanie: algorym weźmie część wartości widma o największej zgromadzonej energii, z pośród częstotliwości
            //króte są większe od wartości minimalnej i zostaną posortowane rosnąco po częstotliwościach
            var ff = table
                .Where(x => x.Frequency > minimumFrequency)
                .OrderByDescending(x => x.Magnitude)
                .Take(fc)
                .OrderBy(x => x.Frequency);

            if (ff == null || ff.Count() == 0)
                return 0;

            //słownik przechowujący częstotliwości kandydujące do bycia częstotliwością bazową i indeksy na 
            //próbki widma pasujące do szeregu
            Dictionary<double, List<int>> frequencies = new Dictionary<double, List<int>>();
            frequencies.Add(ff.ElementAt(0).Frequency, new List<int>() { 0 });

            for (int i = 1; i < ff.Count(); i++)
            {
                double f = ff.ElementAt(i).Frequency;
                bool is_candidate = true;
                foreach (var fb in frequencies.Keys)
                {
                    //porównanie obecnej częstotliwości ze zgromadzonymi
                    //kandydatami do częstotliwości bazowej. Jeżeli wartość jest wielokrotnością,
                    //któreś z kandydatów częstotliwości bazowej, to zwiększa ona szansę na uznanie, że
                    //szereg zapoczątkowany przez kandydata do częstotliwości bazowej za właściwy.
                    //Jeżeli porównywana częstotliwość nie pasuje do szeregu żadnej z częstotliwości kandydującej, to sama
                    //staje się kandydatem do częstotliwosci bazowej

                    if (MathHelper.TryIsIntegerIteration(fb, f, tolerance))
                    {
                        is_candidate = false;
                        frequencies[fb].Add(i);
                        break;
                    }
                }
                if (is_candidate)
                    frequencies.Add(f, new List<int>() { i });
            }

            //sprawdzenie, który szereg zgromadził w sobie największe wartości napięcia
            double max = 0;
            double base_freq = 0;
            foreach (var f in frequencies)
            {
                double series = 0;
                foreach (int i in f.Value)
                    series += ff.ElementAt(i).Magnitude;

                if (series > max)
                {
                    max = series;
                    base_freq = f.Key;
                }
            }

            return base_freq;
        }
    }
}
