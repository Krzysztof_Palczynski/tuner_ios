﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Tuner.Services.Mathematics
{
    public static class MathHelper
    {
        public static bool IsInRange(double a, double b, double radius)
        {
            return (a - radius) <= b && b <= (a + radius);
        }

        /// <summary>
        /// Całka metodą trapezową
        /// </summary>
        /// <param name="values"></param>
        /// <param name="step"></param>
        /// <returns></returns>
        public static double Calculus(this IEnumerable<double> values, double step)
        {
            double toReturn = 0;
            int length = values.Count();

            for (int i = 0; i < length - 1; i++)
            {
                double y1 = values.ElementAt(i);
                double y2 = values.ElementAt(i + 1);

                double trapez = (y1 + y2) * step / 2.0;
                toReturn += trapez;
            }
            toReturn += values.ElementAt(length - 1) * step;


            return toReturn;
        }

        /// <summary>
        /// Ta metoda sprawdza, czy liczba b jest całkowitą wielokrotnością liczby a
        /// </summary>
        /// <param name="a">Mniejsza liczba (większa od 1)</param>
        /// <param name="b">Większa liczba (większa od 1) - wielokrotność</param>
        /// <param name="tolerance">Wartość tolerancji - dopuszczalny błąd, pomimo którego dojdzie do 
        /// uznania wartości za wielokrotność.
        /// Ważne! a - tolerance musi być większe od 1 i b musi być
        /// większe niż a + tolerance</param>
        /// <returns></returns>
        public static bool IsIntegerIteration(double a, double b, double tolerance)
        {
            if (a > b)
                return false;

            if (a <= 1 || b < 1)
                throw new Exception("Liczby muszą być większe od 1");

            if (a + tolerance >= b)
                throw new Exception("Wartość a + tolerance musi być większa od b");

            if (a - tolerance < 1)
                throw new Exception("Wartość a - tolerance musi być większa od 1");

            //określanie granic przedziału

            //najmniejszy możliwy iloraz b_i/a_i 
            //gdzie a_i E<a - tolerance; a + tolerance> i b_i E<b - tolerance ; b + tolerance>
            //to minimalne b podzielone przez maksymalne a i na odwrót w przypadku wartości maksymalnej
            double max = (b + tolerance) / (a - tolerance);
            double min = (b - tolerance) / (a + tolerance);

            double maxMod = System.Math.Floor(max);
            double minMod = System.Math.Floor(min);

            //Warunek pierwszy niekonieczny, wystarczający:
            //Jeżeli, któraś z krawędzi przedziału jest równa wartości
            //funkcji podłogi z tej wartości, to znaczy się, że krawędź
            //jest liczbą całkowitą, czyli są takie dwie wartości, których
            //dzielenie daje w rezultacie liczbę całkowitą, czyli się zgadza
            if (maxMod == max || minMod == min)
                return true;

            //Warunek drugi niekonieczny, wystarczający:
            //Jeżeli wartość funkcja podłogi z wartości maksymalnej jest różna
            //od funkcji podłogi z wartości minimalnej, to znaczy się, że 
            //funkcja F(a,b,tolerance) => Y E (0; nieskończoność), która jest funkcją ciągłą
            //przechodzi przez punkt będący liczbą całkowitą, co oznacza,
            //że przyjmuje wartości mniejsze i większe od tej liczby całkowitej, co 
            //oznacza, że są takie wartości dla których funkcja przyjmuje wartość całkowitą

            if (minMod < maxMod)
                return true;

            return false;
        }

        /// <summary>
        /// Wariant metody, który w przypadku złych wartości zwróci false, zamiast wyjątku
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="tolerance"></param>
        /// <returns></returns>
        public static bool TryIsIntegerIteration(double a, double b, double tolerance)
        {
            try
            {
                return IsIntegerIteration(a, b, tolerance);
            }
            catch (Exception) { return false; }
        }
    }
}
