﻿using System;
using System.IO;
using AVFoundation;
using Foundation;
namespace Tuner.Services
{
    public class RecordService
    {

        public static string FolderPath
        {
            get => Path.GetTempPath();
        }

        readonly NSNumber voice_record_frequency = NSNumber.FromFloat(44100.0f);
        readonly NSNumber tuner_record_frequency = NSNumber.FromFloat(8000.0f);

        AVAudioRecorder _recorder;
        NSDictionary _settings;
        bool _isRecord = false;
        NSUrl _url;
        NSError _error;
        string _path;

        RecordType type;


        public RecordService(RecordType type)
        {
            this.type = type;
        }

        public void Initialize(string path = null, string filename = null)
        {
            var audioSession = AVAudioSession.SharedInstance();
            audioSession.RequestRecordPermission((granted) =>
            {
                if (granted)
                {
                    perform_initialization(audioSession, path, filename);
                }
            });

        }

        private bool perform_initialization(AVAudioSession audioSession, string path, string fileName)
        {
            var err = audioSession.SetCategory(AVAudioSessionCategory.PlayAndRecord);
            if (err != null)
            {
                Console.WriteLine($"Audio error {err}");
                return false;
            }

            if (fileName == null)
                fileName = string.Format("audio{0}.wav", DateTime.Now.ToString("yyyyMMddHHmmss"));
            else
                fileName = $"{fileName}.wav";

            if (string.IsNullOrEmpty(path))
            {
                path = FolderPath;
            }
            string audioFilePath = Path.Combine(path, fileName);
            _path = audioFilePath;
            _url = NSUrl.FromFilename(audioFilePath);

            NSObject[] values = new NSObject[]
            {
                type == RecordType.Tuner ? tuner_record_frequency : voice_record_frequency, //Sample Rate
                NSNumber.FromInt32 ((int)AudioToolbox.AudioFormatType.LinearPCM), //AVFormat
                NSNumber.FromInt32 (type == RecordType.Tuner ? 1 : 2), //Channels
                NSNumber.FromInt32 (16), //PCMBitDepth
                NSNumber.FromBoolean (false), //IsBigEndianKey
                NSNumber.FromBoolean (false) //IsFloatKey
            };

            NSObject[] keys = new NSObject[]
            {
                AVAudioSettings.AVSampleRateKey,
                AVAudioSettings.AVFormatIDKey,
                AVAudioSettings.AVNumberOfChannelsKey,
                AVAudioSettings.AVLinearPCMBitDepthKey,
                AVAudioSettings.AVLinearPCMIsBigEndianKey,
                AVAudioSettings.AVLinearPCMIsFloatKey
            };

            _settings = NSDictionary.FromObjectsAndKeys(values, keys);

            _recorder = AVAudioRecorder.Create(_url, new AudioSettings(_settings), out _error);
            return _recorder.PrepareToRecord();
        }

        public bool Record(string path = null)
        {
            if (_recorder == null || _isRecord)
            {
                return false;
            }

            //if (!Initialize(path))
            //    return false;

            _recorder.Record();
            _isRecord = true;
            return true;
        }

        public bool IsRecording()
        {
            return _isRecord;
        }

        public byte[] StopRecording(bool deleteFile)
        {
            if (_recorder == null || !_isRecord)
            {
                return default(byte[]);
            }

            _recorder.Stop();
            _isRecord = false;

            var bytes = default(byte[]);
            using (var reader = new StreamReader(_path))
            {
                using (var memstream = new MemoryStream())
                {
                    reader.BaseStream.CopyTo(memstream);
                    bytes = memstream.ToArray();
                }
            }

            if (deleteFile)
            {
                File.Delete(_path);
            }

            return bytes;
        }

        public void PlaySound(Action finishedPlaying = null)
        {
            var player = new AVAudioPlayer(_url, "Song", out _error);
            player.Volume = 1.0f;
            player.FinishedPlaying += (sender, e) => finishedPlaying?.Invoke();

            player.Play();
        }
    }

    public enum RecordType
    {
        Tuner = 0,
        Voice = 1
    }
}
