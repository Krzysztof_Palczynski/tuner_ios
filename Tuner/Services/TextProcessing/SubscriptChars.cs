﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tuner.Services.TextProcessing
{
    public class SubscriptChars
    {
        public static Dictionary<int, char> SubscriptNumbers = new Dictionary<int, char>()
        {
            {0, '\u2080' },
            {1 , '\u2081'},
            {2 , '\u2082'},
            {3 , '\u2083'},
            {4 , '\u2084'},
            {5 , '\u2085'},
            {6 , '\u2086'},
            {7 , '\u2087'},
            {8 , '\u2088'},
            {9 , '\u2089'}
        };

        public static string ConvertToSubscript(string s)
        {
            if (s == null)
                return "";

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < s.Length; i++)
            {
                int a = 0;
                if (int.TryParse(s[i] + "", out a))
                {
                    if (SubscriptNumbers.ContainsKey(a))
                        sb.Append(SubscriptNumbers[a]);
                    else
                        sb.Append(s[i]);
                }
                else
                    sb.Append(s[i]);
            }

            return sb.ToString();
        }
    }
}
