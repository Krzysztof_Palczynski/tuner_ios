// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Tuner
{
    [Register ("RecordController")]
    partial class RecordController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField filenameTextField { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton playRecordingButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel RecordingTimeLabel { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton startRecordingButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton stopRecordingButton { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (filenameTextField != null) {
                filenameTextField.Dispose ();
                filenameTextField = null;
            }

            if (playRecordingButton != null) {
                playRecordingButton.Dispose ();
                playRecordingButton = null;
            }

            if (RecordingTimeLabel != null) {
                RecordingTimeLabel.Dispose ();
                RecordingTimeLabel = null;
            }

            if (startRecordingButton != null) {
                startRecordingButton.Dispose ();
                startRecordingButton = null;
            }

            if (stopRecordingButton != null) {
                stopRecordingButton.Dispose ();
                stopRecordingButton = null;
            }
        }
    }
}