using Foundation;
using System;
using System.Collections.Generic;
using Tuner.Observers;
using Tuner.Services.SoundProcessing.FFT;
using UIKit;

namespace Tuner
{
    public partial class HarmonicsListController : UITableViewController
    {
        private IHarmonicsObserver observer;
        public IHarmonicsObserver Observer
        {
            get
            {
                return observer;
            }
            set
            {
                observer = value;
                observer.Updated += (sender, e) => { 
                    Harmonics = e;
                    TableView.ReloadData();
                };

            }
        }


        public List<Harmonic> Harmonics { get; private set; }

        public static NSString CellId = new NSString("CallHistoryCell");

        public HarmonicsListController (IntPtr handle) : base (handle)
        {
            Harmonics = new List<Harmonic>();
            TableView.RegisterClassForCellReuse(typeof(UITableViewCell), CellId);
            TableView.Source = new HarmonicsDataSource(this);
        }
    }

    class HarmonicsDataSource : UITableViewSource
    {
        HarmonicsListController c;

        public HarmonicsDataSource(HarmonicsListController controller)
        {
            this.c = controller;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = tableView.DequeueReusableCell(HarmonicsListController.CellId);
            int row = indexPath.Row;
            var h = c.Harmonics[row];
            var percentage = Math.Floor(h.SpectrumCoverage * 1000) / 10;
            cell.TextLabel.Text = $"{Math.Floor(h.Frequency)}Hz - Zajętość widma: {percentage}%";
            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return c.Harmonics.Count;
        }
    }

    public class Harmonic
    {
        public double Frequency { get; set; }
        public double SpectrumCoverage { get; set; }
    }
}