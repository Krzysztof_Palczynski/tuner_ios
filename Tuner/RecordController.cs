using Foundation;
using System;
using System.Text;
using System.Threading;
using Tuner.Services;
using UIKit;

namespace Tuner
{
    public partial class RecordController : UIViewController
    {
        RecordService record;
        Timer timer;

        int secounds = 0;

        public RecordController (IntPtr handle) : base (handle)
        {
            record = new RecordService(RecordType.Voice);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            this.startRecordingButton.TouchUpInside += (sender, e) => {

                //start recording
                record.Initialize(filename: this.filenameTextField.Text);
                record.Record();
                secounds = 0;
                update_secounds_label(secounds);
                timer = new Timer((s) => update_secounds_label(++secounds), null, 1000, 1000);

                //udapte gui
                this.startRecordingButton.UserInteractionEnabled = false;
                this.filenameTextField.UserInteractionEnabled = false;
                this.stopRecordingButton.UserInteractionEnabled = true;
                this.playRecordingButton.UserInteractionEnabled = false;
            };

            this.stopRecordingButton.TouchUpInside += (sender, e) => stopRecording();

            this.playRecordingButton.TouchUpInside += (sender, e) => {

                record.PlaySound();

            };
        }

        private void stopRecording()
        {
            //stop recording
            record?.StopRecording(false);
            timer?.Dispose();

            //update gui
            this.startRecordingButton.UserInteractionEnabled = true;
            this.filenameTextField.UserInteractionEnabled = true;
            this.stopRecordingButton.UserInteractionEnabled = false;
            this.playRecordingButton.UserInteractionEnabled = true;
        }

        private void update_secounds_label(int secoudns)
        {
            var minutes = secounds / 60;
            var secs = secoudns % 60;

            StringBuilder sb = new StringBuilder();
            if (minutes < 10)
                sb.Append("0");
            sb.Append(minutes);
            sb.Append(":");
            if (secs < 10)
                sb.Append("0");
            sb.Append(secs);

            UIApplication.SharedApplication.BeginInvokeOnMainThread(() => {
                this.RecordingTimeLabel.Text = sb.ToString();
            });
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);

            stopRecording();
        }
    }
}