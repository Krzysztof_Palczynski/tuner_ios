using Foundation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Tuner.Services;
using UIKit;

namespace Tuner
{
    public partial class RecordsListController : UITableViewController
    {
        public List<string> filenames;

        public static NSString CellId = new NSString("CallHistoryCell");

        public RecordsListController (IntPtr handle) : base (handle)
        {
            filenames = new List<string>();
            TableView.RegisterClassForCellReuse(typeof(UITableViewCell), CellId);
            TableView.Source = new RecordsDataSource(this);
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            filenames = Directory.GetFiles(RecordService.FolderPath)
                .Where(x => Path.GetExtension(x) == ".wav" && x.Substring(0, 9) != "audio2019")
                .Select(x => Path.GetFileNameWithoutExtension(x))
                .ToList();

            TableView.ReloadData();
        }
    }

    class RecordsDataSource : UITableViewSource
    {
        RecordsListController c;

        public RecordsDataSource(RecordsListController c)
        {
            this.c = c;
        }

        public override nint RowsInSection(UITableView tableView, nint section)
        {
            return c.filenames.Count;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = tableView.DequeueReusableCell(RecordsListController.CellId);
            int row = indexPath.Row;
            cell.TextLabel.Text = c.filenames[row];
            return cell;
        }
    }
}