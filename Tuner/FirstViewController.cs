﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DeviceCheck;
using Foundation;
using Tuner.Observers;
using Tuner.Services;
using Tuner.Services.SoundProcessing;
using Tuner.Services.SoundProcessing.FFT;
using UIKit;

namespace Tuner
{
    public partial class FirstViewController : UIViewController
    {
        RecordService recorder;
        bool initialized = false;
        bool recording = false;

        HarmonicsUpdater Harmonics;

        public FirstViewController(IntPtr handle) : base(handle)
        {
            recorder = new RecordService(RecordType.Tuner);
            Harmonics = new HarmonicsUpdater();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.



            this.recordButton.TouchUpInside += async (sender, e) => {

                recording = !recording;

                if (!initialized)
                {
                    recorder.Initialize();
                    initialized = true;
                }
                if(recording)
                    record_sound();

                this.recordButton.TitleLabel.Text = recording ? "Rozpocznij strojenie" : "Zakończ strojenie";

            };


        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        private async void record_sound()
        {
            recorder.Record();
            await Task.Delay(1000);
            var sound = recorder.StopRecording(false);

            if (!recording)
                return;

            //convert to complex numbers
            ComplexNumber[] samples = new ComplexNumber[sound.Length / 2];
            for(int i = 0; i < sound.Length / 2; i++)
            {
                samples[i] = new ComplexNumber() { real = (double)BitConverter.ToInt16(sound, i * 2), imaginary = 0 };
            }

            //calculate fourier
            var fft = FastFourierTransformAlgorithm.CalculateFFT(samples, (double)sound.Length / 2 / 2)
                .Where(x => x.Frequency > 70 && x.Frequency < 1400)
                .GroupBy(x => Math.Floor(x.Frequency))
                .Select(x => new FFTPoint() { 
                    Frequency = x.Key,
                    Magnitude = x.Sum(y=>y.Magnitude)
                })
                .ToArray();


            //test parameters
            var max = fft.Max(x => x.Magnitude);

            if (max < 80)
            {
                //if maximum magnitude is below 80 it means, that SNR is too low for reading
                //update gui
                UIApplication.SharedApplication.BeginInvokeOnMainThread(() =>
                {
                    this.tuneNameLabel.Text = "-";
                    this.tuneFrequencyLabel.Text = "-";
                });

            }
            else
            {

                var point = fft.First(x => x.Magnitude == max);
                var index = fft.ToList().IndexOf(point);
                Console.WriteLine($"index: {index}, frequency: {point.Frequency}, magnitude: {point.Magnitude}");

                //get base frequency
                var baseFrequency = SoundRecognizer.FindAcousticResonanceFrequency(fft, 1, 70);

                //getting harmonics
                var energy = fft.Sum(x => x.Magnitude);
                Harmonics.ModifyObservedItem(
                    fft
                    .OrderByDescending(x=>x.Magnitude)
                    .Take(20)
                    .ToList()
                    .ConvertAll(x => new Harmonic() { 
                         Frequency = x.Frequency,
                         SpectrumCoverage = x.Magnitude / energy
                    })
                );

                //get tune name
                var tune = AcousticSounds.RecognizeSound(baseFrequency, Tuning.T432Hz);
                var tuneName = tune.HasValue ? tune.Value.GetSoundName() : null;
                var tuneIdealFrequency = Math.Floor(tune.HasValue ? tune.Value.GetSoundFrequency(Tuning.T432Hz) : 0);

                //update gui
                UIApplication.SharedApplication.BeginInvokeOnMainThread(() =>
                {
                    this.tuneNameLabel.Text = tuneName;
                    this.tuneFrequencyLabel.Text = $"{baseFrequency}Hz / {tuneIdealFrequency}Hz";
                });
            }

            if (recording)
                record_sound();
        }

        public override void PrepareForSegue(UIStoryboardSegue segue, NSObject sender)
        {
            base.PrepareForSegue(segue, sender);

            var hc = segue.DestinationViewController as HarmonicsListController;
            if(hc != null)
            {
                hc.Observer = Harmonics;
            }
            if (segue.DestinationViewController is RecordController)
                recording = false;
        }
    }
}